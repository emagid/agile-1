<?php

class highlightsController extends adminController {
	
	function __construct(){
		parent::__construct("Highlight", "highlights");
	}

	function index(Array $params = []){
		$this->_viewData->hasCreateBtn = true;		

		parent::index($params);
	}

	function update(Array $arr = []){
		

		parent::update($arr);
	}

	function update_post(Array $arr = []){

		parent::update_post($arr);
	}

}