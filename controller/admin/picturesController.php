<?php

class picturesController extends adminController {
	
	function __construct(){
		parent::__construct("Snapshot_Contact","pictures");
	}
    public function update_post()
    {
        $obj = \Model\Snapshot_Contact::getItem($_POST['id']);
        $obj->in_slider = $_POST['in_slider'];

        if ($obj->save()) {
            $this->toJson(['status'=>true]);
        }
    }

}