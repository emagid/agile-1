<?php

use Twilio\Rest\Client;

class homeController extends siteController {
        function __construct(){
        parent::__construct();
    }    
    public function index(Array $params = []){
        $this->loadView($this->viewData);
    }

    function photobooth(Array $params = []){
        $this->loadView($this->viewData);
    }
    
    function speakers(Array $params = []){
        $this->loadView($this->viewData);
    }

    function agenda(Array $params = []){
        $this->loadView($this->viewData);
    }

    function sponsors(Array $params = []){
        $this->loadView($this->viewData);
    }

    function allsponsors(Array $params = []){
        $this->loadView($this->viewData);
    }

    function floorplan(Array $params = []){
        $this->loadView($this->viewData);
    }

    function awardees(Array $params = []){
        $this->loadView($this->viewData);
    }

    public function submit_survey(){
        $resp['status'] = false;
        if(isset($_POST) && $_POST != ''){
            $survey_answer = \Model\Survey::loadFromPost();
            if($survey_answer->save()){
                $resp['status'] = true;
            }
        }
        $this->toJson($resp);
    }
   
}