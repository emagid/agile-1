<?php
/**
 * Created by PhpStorm.
 * User: Foran
 * Date: 9/25/2017
 * Time: 9:23 AM
 */

class triviaController extends siteController {
    function __construct()
    {
        parent::__construct();
    }

    function index(Array $params = [])
    {
        $questions = \Model\Question::getList(['orderBy'=>'random()']);
        foreach ($questions as $i=> $question){
            $questions[$i]->answers = $question->get_answers();
        }
        $this->viewData->questions = $questions;

        $this->loadView($this->viewData);
    }

    function trivia2(Array $params = [])
    {
        $questions = \Model\Question::getList(['orderBy'=>'random()']);
        foreach ($questions as $i=> $question){
            $questions[$i]->answers = $question->get_answers();
        }
        $this->viewData->questions = $questions;

        $this->loadView($this->viewData);
    }
}