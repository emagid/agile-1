<?php
/**
 * Created by PhpStorm.
 * User: Janus
 * Date: 11/10/15
 * Time: 3:13 PM
 */

namespace Services;
require_once(__DIR__.'/../usaepay-php/usaepay.php');
use Model\Order;
/**
 * Class PaymentService
 * Simple wrapper for USA epay
 * @package Services
 */
class PaymentService
{
    public $tran;
    private $error = [];
    public function __construct()
    {
        $this->tran = new \umTransaction();
        $this->tran->key = USAEPAY_KEY;

        if(defined('USAEPAY_PIN')){
            $this->tran->pin = USAEPAY_PIN;
        }
        if(defined('USAEPAY_SANDBOX')){
            $this->tran->usesandbox = USAEPAY_SANDBOX;
        }

        if(defined('USAEPAY_TESTMODE')){
            $this->tran->testmode = USAEPAY_TESTMODE;
        }
    }

    public function setCard($card)
    {
        $this->tran->card = $card;
        return $this;
    }

    /**
     * Payment expiration format: mm/yy
     * @param $month
     * @param $year
     * @return $this
     */
    public function setExp($month, $year)
    {
        $month = sprintf('%02d', $month);
        if(strlen($year) == 4){
            $year = substr($year, -2);
        }
        $this->tran->exp = $month.$year;
        return $this;
    }

    /**
     * Set payment amount, acceptable param should be either 5.6, '5.6', or '$5.6'
     * @param $amount
     * @return $this|PaymentService
     */
    public function setAmount($amount)
    {
        if(!is_numeric($amount)){
            if(substr($amount, 0, 1) == '$'){
                $amount = substr($amount, 0, 1);
                return $this->setAmount($amount);
            } else {
                $this->error[] = ['message' => 'Invalid Amount'];
            }
        } else {
            $this->tran->amount = $amount;
        }

        return $this;
    }


    /**
     * Put our own internel order refer number or id
     * @param $orderId
     * @return $this
     */
    public function setOrderId($orderId)
    {
        $this->tran->orderid = $orderId;
        return $this;
    }

    public function setPin($pin)
    {
        $this->tran->pin = $pin;
        return $this;
    }

    public function setBillingAddress(Order $order)
    {
        $this->tran->billfname = $order->bill_first_name;
        $this->tran->billlname = $order->bill_last_name;
        $this->tran->billstreet = $order->bill_address;
        $this->tran->billstreet2 = $order->bill_address2;
        $this->tran->billcity = $order->bill_city;
        $this->tran->billstate = $order->bill_state;
        $this->tran->billzip = $order->bill_zip;
        $this->tran->billcountry = $order->bill_country;
        $this->tran->billphone = $order->phone;
        $this->tran->email = $order->email;

        return $this;
    }

    public function setCCV($ccv)
    {
        $this->tran->ccv = $ccv;
        Return $this;
    }

    public function setCommand($command){
        $this->tran->command = $command;
        return $this;
    }

    public function setRefNum($refNum){
        $this->tran->refnum = $refNum;
        return $this;
    }

    public function process()
    {
        if($this->error){
            return false;
        } else {
            $this->tran->process();
        }
    }

    public function getError()
    {
        if($this->error){
            return $this->error;
        } else {
            return [['message' => $this->tran->error]];
        }
    }

    public function isSuccess()
    {
        return !$this->error && ($this->tran->result == 'Approved' || $this->tran->result == 'Verification');
    }

    public function getResult()
    {
        return $this->tran->result;
    }

    public function getRefNum()
    {
        return $this->tran->refnum;
    }
}