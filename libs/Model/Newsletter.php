<?php

namespace Model;

class Newsletter extends \Emagid\Core\Model
{
	static $tablename = "newsletter";
	public static $fields = [
			'email' => ['required' => true, 'type' => 'email'],
			'category',
			'user_id',
			'is_subscribe'
	];

	public static function getCategories()
	{
		return ['Whole Sale', 'New Products', 'Promotions', 'Coupons'];
	}
}
