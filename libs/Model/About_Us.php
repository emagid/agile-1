<?php

namespace Model;

class About_Us extends \Emagid\Core\Model {
    static $tablename = "public.about_us";

    public static $fields  =  [
        'description',
    ];

}