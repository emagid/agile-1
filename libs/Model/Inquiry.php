<?php

namespace Model;

class Inquiry extends \Emagid\Core\Model {
    static $tablename = "public.inquiry";

    public static $fields  =  [
        'name',
        'email',
        'phone',
        'message',
        'prefer',
        'metal_shape',
        'size',
        'carat',
        'color',
        'clarity',
        'budget',

    ];
}















