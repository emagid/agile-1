<div role="tabpanel">
	<ul class="nav nav-tabs" role="tablist">
	  	<li role="presentation" class="active"><a href="#account-info-tab" aria-controls="general" role="tab" data-toggle="tab">Account Information</a></li> 
	</ul>
	<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data" >
		<input type="hidden" name="id" value="<?php echo $model->banner->id;?>" /> 
		<div class="tab-content">
      		<div role="tabpanel" class="tab-pane active" id="account-info-tab">
          		<div class="row">
              		<div class="col-md-12">
                  		<div class="box">
							<h4>Account Information</h4>
							 <div class="form-group">
								<label>Title</label>
								<?php echo $model->form->editorFor('title');?>
							</div>
							<div class="form-group">
								<label>Link</label>
								<?php echo $model->form->editorFor('link');?>
							</div>
							<div class="form-group">
								<label>Featured</label><small> (Set to 0 to add main banner)</small>
								<?php echo $model->form->editorFor('featured_id');?>
							</div>
							<div class="form-group" id="banner_order_cont">
								<label>Banner Order</label><small> (For main banner)</small>
<!--								--><?php //echo $model->form->editorFor('featured_id');?>
								<input id="banner_order" type="text" name="banner_order" value="<?=$model->banner->banner_order?>"/>
							</div>
							 <div class="form-group">
								<label>Image</label>
								<p><small>(ideal profile photo size is 850 x 850)</small></p>
								<p><input type="file" name="image" class='image' /></p>
								<div style="display:inline-block">
									<?php 
										$img_path = "";
										if($model->banner->image != "" && file_exists(UPLOAD_PATH.'banners'.DS.$model->banner->image)){ 
											$img_path = UPLOAD_URL . 'banners/' . $model->banner->image;
									?>
											<div class="well well-sm pull-left" style="max-width:106.25px;max-height:106.25px;">
												<img src="<?php echo $img_path; ?>" />
												<br />
												<a href="<?= ADMIN_URL.'banners/delete_image/'.$model->banner->id;?>?photo=1" onclick="return confirm('Are you sure?');" class="btn btn-default btn-xs">Delete</a>
												<input type="hidden" name="image" value="<?=$model->banner->image?>" />
											</div>
									<?php } ?>
									<div class='preview-container' style="max-width:106.25px;max-height:106.25px;"></div>
								</div>
							</div>
                  		</div>
              		</div>
              		 
          		</div>
        	</div>
        	 
        	 
        	 
				 
			</div>
        </div>
        <div class="form-group">
			<button type="submit" class="btn btn-save">Save</button>
		</div>
     </form>
<div class="box" style="margin-top: 15px;">
	<h4>Home Map</h4>
	<div class="home-map">
		<div class="top"><div class="container">TOP</div></div>
		<div class="header"><div class="container">HEADER</div></div>
		<? $style = ' style="border-style:solid; background: #ccc; font-weight: bold;" '; ?>
		<div class="main-banner" <?=($model->banner->featured_id == 0)?$style:''?> ><div class="container">MAIN BANNER</div></div>
		<div class="featured-banner">
			<div class="container">
				<div class="col-sm-8">
					<div id="f-1" <?=($model->banner->featured_id == 1)?$style:''?> >FEATURED #1</div>
					<div id="f-2" <?=($model->banner->featured_id == 2)?$style:''?> >FEATURED #2</div>
				</div>
				<div class="col-sm-8">
					<div id="f-3" <?=($model->banner->featured_id == 3)?$style:''?> >FEATURED #3</div>
					<div id="f-4" <?=($model->banner->featured_id == 4)?$style:''?> >FEATURED #4</div>
				</div>
				<div class="col-sm-8">
					<div id="f-5" <?=($model->banner->featured_id == 5)?$style:''?> >FEATURED #5</div>
					<div id="f-6" <?=($model->banner->featured_id == 6)?$style:''?> >FEATURED #6</div>
				</div>
			</div>
		</div>
		<div class="deal-of-the-week"><div class="container">DEAL OF THE WEEK</div></div>
		<div class="news"><div class="container">RECENT NEWS</div></div>
		<div class="footer"><div class="container">FOOTER</div></div>
	</div>
</div>
</div>

<?php footer();?>

<script>

function readURL(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();
		var img = $("<img />");
		reader.onload = function (e) {
			img.attr('src',e.target.result);
			img.attr('alt','Uploaded Image');
			img.attr("width",'100%');
			img.attr('height','100%');
		};
		$(input).parent().parent().find('.preview-container').html(img);
		$(input).parent().parent().find('input[type="hidden"]').remove();

		reader.readAsDataURL(input.files[0]);
	}
}

$(function(){

	$("select.multiselect").each(function(i,e) {
        //$(e).val('');
        var placeholder = $(e).data('placeholder');
        $(e).multiselect({
            nonSelectedText:placeholder,
            includeSelectAllOption: true,
            maxHeight: 415,
            checkboxName: '',
            enableCaseInsensitiveFiltering: true,
            buttonWidth: '100%'
        });
    });

	$("input.image").change(function(){
		readURL(this);
	});
});

$('input[name=featured_id]').on('keyup',function(){
	if($(this).val() != 0 || $.trim($(this).val()) == ''){
		$('#banner_order_cont').hide();
		$('#banner_order').prop('disabled',true);
	} else {
		$('#banner_order_cont').show();
		$('#banner_order').prop('disabled',false);
	}
});

$('input[name=featured_id]').trigger('keyup')

</script>