<?php
$states = get_states();
unset($states['GU']);unset($states['PR']);unset($states['VI']);
?>
<div role="tabpanel">
	<ul class="nav nav-tabs" role="tablist">
		<li role="presentation" class="active"><a href="#account-info-tab" aria-controls="general" role="tab" data-toggle="tab">Account Information</a></li>
		<?php if($model->user->id>0) { ?>
			<li role="presentation"><a href="#addresses-tab" aria-controls="seo" role="tab" data-toggle="tab">Delivery Addresses</a></li>
			<li role="presentation"><a href="#timesheet-tab" aria-controls="categories" role="tab" data-toggle="tab">Payment</a></li>
		<?php } ?>
	</ul>
	<div class="tab-content">
		<div role="tabpanel" class="tab-pane active" id="account-info-tab">
			<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data" >
				<input type="hidden" name="id" value="<?php echo $model->user->id;?>" />
				<input type=hidden name="token" value="<?php echo get_token(); ?>" />
				<div class="row">
					<div class="col-md-12">
						<div class="box">
							<h4>Account Information</h4>
							<div class="form-group">
								<label>Email Address:</label>
                                <?php echo $model->form->editorFor("email");?>
                                <input type="hidden" name="wholesale_id" value="<?=$model->user->wholesale_id?>">
							</div>
							<div class="form-group">
								<label>Username:</label>
                                <?php echo $model->form->editorFor("username");?>
							</div>
							<div class="form-group">
								<label>First Name</label>
                                <?php echo $model->form->editorFor("first_name");?>
							</div>
							<div class="form-group">
								<label>Last Name</label>
                                <?php echo $model->form->editorFor("last_name");?>
							</div>
                            <div class="form-group">
                                <label>Picture</label>

                                <p>
                                    <small>(Ideal picture size is 500 x 300)</small>
                                </p>
                                <p><input type="file" name="featured_image" class='image'/></p>

                                <div style="display:inline-block">
                                    <?php
                                    $img_path = "";
                                    if ($model->user->photo != "" && file_exists(UPLOAD_PATH . 'users' . DS . $model->user->photo)) {
                                        $img_path = UPLOAD_URL . 'users/' . $model->user->photo;
                                        ?>
                                        <div class="well well-sm pull-left">
                                            <img src="<?php echo $img_path; ?>" width="100"/>
                                            <br/>
                                            <a href="<?= ADMIN_URL . 'users/delete_image/' . $model->users->id; ?>?featured_image=1"
                                               onclick="return confirm('Are you sure?');"
                                               class="btn btn-default btn-xs">Delete</a>
                                            <input type="hidden" name="photo"
                                                   value="<?= $model->user->photo ?>"/>
                                        </div>
                                    <?php } ?>
                                    <div class='preview-container'></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Phone</label>
                                <?php echo $model->user->phone;?>
                            </div>
                            <div class="form-group">
								<label>Password (leave blank to keep current password)</label>
								<input type="password" name="password" />
							</div>
							<div class="form-group">
								<button type="submit" class="btn btn-save">Save</button>
							</div>
						</div>
					</div>
				</div>

			</form>
		</div>
		<?php if($model->user->id>0) { ?>
			<!--<div role="tabpanel" class="tab-pane" id="addresses-tab">
				<div class="row">
					<div class="col-md-24">
						<div class="box">
							<a href="<?php /*echo ADMIN_URL.'users/address/-1?user_id='.$model->user->id;*/?>" class="btn btn-primary btn-add">Add New Address</a>
						</div>
					</div>
				</div>
				<div class="row">
					<?php /*foreach($model->addresses as $addr) { */?>
						<div class="col-md-6">
							<div class="box">
								<div class="form-group">
									<label>Label: </label>
									<?php /*echo $addr->label;*/?>
								</div>
								<div class="form-group">
									<label>First Name: </label>
									<?php /*echo $addr->first_name;*/?>
								</div>

								<div class="form-group">
									<label>Last Name: </label>
									<?php /*echo $addr->last_name;*/?>
								</div>
								<?php /*if ($addr->phone!="") { */?>
									<div class="form-group">
										<label>Phone Number: </label>
										<?php /*echo $addr->phone;*/?>
									</div>
								<?php /*} */?>
								<div class="form-group">
									<label>Address: </label>
									<?php /*echo $addr->address;*/?>
								</div>

								<div class="form-group">
									<label>Address 2: </label>
									<?php /*echo $addr->address2;*/?>
								</div>

								<div class="form-group">
									<label>City: </label>
									<?php /*echo $addr->city;*/?>
								</div>

								<div class="form-group">
									<label>State: </label>

									<?php /*echo $states[$addr->state];*/?>
								</div>

								<div class="form-group">
									<label>Zip Code: </label>
									<?php /*echo $addr->zip;*/?>
								</div>

								<div class="form-group">
									<label>Country: </label>
									<?php /*echo $addr->country;*/?>
								</div>

								<div class="form-group">
									<a class="btn-actions" href="<?php /*echo ADMIN_URL; */?>users/address/<?php /*echo $addr->id; */?>?user_id=<?php /*echo $model->user->id;*/?>">
										<i class="icon-pencil"></i>
									</a>
									<a class="btn-actions" href="<?php /*echo ADMIN_URL; */?>users/address_delete/<?php /*echo $addr->id; */?>?token_id=<?php /*echo get_token();*/?>?user_id=<?php /*echo $model->user->id;*/?>" onClick="return confirm('Are You Sure?');">
										<i class="icon-cancel-circled"></i>
									</a>
								</div>
							</div>
						</div>
					<?php /*} */?>
				</div>
			</div>-->

		<?php } ?>
	</div>
</div>
<?php footer();?>

<script type="text/javascript">
	var site_url = "<?php echo ADMIN_URL;?>users/";
	var user_id = <?php echo $model->user->id;?>;
	jQuery.validator.addMethod("validate_user_email",function(value,element) {
		var return_val = false;

		var data = {email:value,id: user_id};
		$.ajax({
			url: site_url+"users/validate_user_email",
			async: false,
			data: data,
			method: "GET",
			success: function(response) {
				if(response.success) {
					return_val = true;
				}
			}

		});
		return return_val;
	},"Email is invalid or already being used.");

	$("#user-form").validate({
		onkeyup: false,
		onfocusout: false,
		onclick: false,
		focusInvalid :false,
		rules: {
			email: {required:true},
			first_name: {required:true},
			last_name: {required:true},
		},
	});
	$('.coupon_send').change(function() {
		var product_id = $(this).attr("product");
		var user_id = $(this).attr("user");
		var coupon_id = $(this).val();



		$.ajax({
			url: '/admin/users/send_coupon/',
			method: 'POST',
			data: {
				product_id: product_id, user_id:user_id, coupon_id: coupon_id
			},
			success: function(data) {
				alert(data);

				/* window.location.replace("/checkout/");*/
			}
		});
	});
	$(".phone-us").inputmask("(999)999-9999");
</script>
<script type="text/javascript">
	$(document).ready(function () {
		$("input[name='name']").on('keyup', function (e) {
			var val = $(this).val();
			val = val.replace(/[^\w-]/g, '-');
			val = val.replace(/[-]+/g, '-');
			$("input[name='slug']").val(val.toLowerCase());
		});

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                var img = $("<img />");
                reader.onload = function (e) {
                    img.attr('src', e.target.result);
                    img.attr('alt', 'Uploaded Image');
                    img.attr("width", '100');
                    img.attr('height', '100');
                };
                $(input).parent().parent().find('.preview-container').html(img);
                $(input).parent().parent().find('input[type="hidden"]').remove();

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("input.image").change(function () {
            readURL(this);
            console.log("triggered");
        });

		$("select.multiselect").each(function (i, e) {
//            $(e).val('');
			var placeholder = $(e).data('placeholder');
			$(e).multiselect({
				nonSelectedText: placeholder,
				includeSelectAllOption: true,
				maxHeight: 415,
				checkboxName: '',
				enableCaseInsensitiveFiltering: true,
				buttonWidth: '100%'
			});
		});
		$('select.multiselect').multiselect('rebuild');
	});
</script>