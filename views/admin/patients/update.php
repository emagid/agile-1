<div role="tabpanel">
	<ul class="nav nav-tabs" role="tablist">
		<li role="presentation" class="active"><a href="#details-info-tab" aria-controls="categories" role="tab" data-toggle="tab">Details</a></li>
		<!--		<li role="presentation"><a href="#patients-info-tab" aria-controls="general" role="tab" data-toggle="tab">Patients</a></li>-->
		<!--		<li role="presentation"><a href="#appointment-tab" aria-controls="seo" role="tab" data-toggle="tab">Appointments</a></li>-->
	</ul>
	<div class="tab-content">
		<div role="tabpanel" class="tab-pane active" id="details-info-tab">
			<div class="row">
				<div class="col-md-12">
					<div class="box">
						<h4>Patient Information</h4>
						<div class="form-group">
							<label>Name:</label>
							<p><?="{$model->patient->LN}, {$model->patient->FN}".(!empty($model->patient->MI) ? ' '.$model->patient->MI : '' )?></p>
						</div>
						<div class="form-group">
							<label>Email:</label>
							<p><?=$model->patient->EM ?></p>
						</div>
						<div class="form-group">
							<label>Home Phone:</label>
							<p><?=$model->patient->HP ?></p>
						</div>
						<div class="form-group">
							<label>Cell Phone:</label>
							<p><?=$model->patient->CP ?></p>
						</div>
						<?if(isset($model->res_party)) {?>
						<div class="form-group">
							<label>Responsible Party:</label>
							<p><a href="<?php echo ADMIN_URL; ?>patients/update/<?=$model->patient->RID?>"><?="{$model->res_party->LN}, {$model->res_party->FN}".(!empty($model->res_party->MI) ? ' '.$model->res_party->MI : '' )?></a></p>
						</div>
						<? } ?>
						<div class="form-group">
							<label>Patient of Office:</label>
							<p><span class="office-name" data-office="<?=$model->patient->PID ?>"><?=$model->patient->PID ?></span></p>
						</div>
						<div class="form-group">
							<label>First Visit:</label>
							<p><?=$model->patient->FV ?></p>
						</div>
						<div class="form-group">
							<label>Last Visit:</label>
							<p><?=date('m/d/y',strtotime($model->patient->LV))?></p>
						</div>
						<div class="form-group">
							<label>Email:</label>
							<p><?=$model->patient->EM ?></p>
						</div>
						<div class="form-group">
							<label>Phone Numbers:</label>
							<p>Home #:<?=$model->patient->HP ?></p>
							<p>Cell #:<?=$model->patient->CP ?></p>
						</div>
						<div class="form-group">
							<label>Address:</label>
							<p><?=$model->patient->AD ?></p>
							<p><?=$model->patient->AD2 ?></p>
							<p><?=$model->patient->CT.", ".$model->patient->ST ?></p>
							<p><?=$model->patient->ZP ?></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>