<main class='content'>
	<a class='home_btn' href="/"><img src="<?= FRONT_ASSETS ?>img/home.png"></a>
	<main class='background'>
		<section class="agenda">
	        <div class='cards'>
	            <div data-id='agenda1' class='card half open'>
	                <p>Guests</p>
	            </div>
	            <div data-id='agenda2' class='card half'>
	                <p>Suppliers</p>
	            </div>
	                <img class='img' id='agenda1' src="<?= FRONT_ASSETS ?>img/map1.jpg">
	                <img class='img' id='agenda2' src="<?= FRONT_ASSETS ?>img/map2.jpg">
	        </div>
	    </section>
	</main>

	<script type="text/javascript">
	    $(document).on('click', '.card', function(){
	    	$(this).addClass('enlarge');
	    	$('.card').removeClass('open');
	        $(this).addClass('open');
	        var id = $(this).attr('data-id');
	        $('.img').fadeOut(500);
	        setTimeout(function(){
	            $('#' + id).fadeIn(500);
	        }, 300);
	    });
	</script>
</main>