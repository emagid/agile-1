<main class='content'>
	<a class='home_btn' href="/"><img src="<?= FRONT_ASSETS ?>img/home.png"></a>
	<main class='background'>
		<section class="awardees">
	        <div class='cards logos'>
	            <div data-id='logo_amp' class='card'>
	                <img src="<?= FRONT_ASSETS ?>img/sponsors/img1.jpg">
	            </div>
	            <div data-id='logo_OnBoardServices' class='card'>
	                <img src="<?= FRONT_ASSETS ?>img/sponsors/img6.jpg">
	            </div>
	            <div data-id='logo_compunnel' class='card'>
	                <img src="<?= FRONT_ASSETS ?>img/sponsors/img2.jpg">
	            </div>
	            <div data-id='logo_enterprise' class='card'>
	                <img src="<?= FRONT_ASSETS ?>img/sponsors/img3.jpg">
	            </div>
	            <div data-id='logo_eteam' class='card'>
	                <img src="<?= FRONT_ASSETS ?>img/sponsors/img4.jpg">
	            </div>
	            <div data-id='logo_fountainGroup' class='card'>
	                <img src="<?= FRONT_ASSETS ?>img/sponsors/img5.jpg">
	            </div>
	            <div data-id='logo_OnBoardServices' class='card'>
	                <img src="<?= FRONT_ASSETS ?>img/sponsors/img6.jpg">
	            </div>
	            <div data-id='logo_Radiant' class='card'>
	                <img src="<?= FRONT_ASSETS ?>img/sponsors/img7.jpg">
	            </div>
	            <div data-id='logo_TPG' class='card'>
	                <img src="<?= FRONT_ASSETS ?>img/sponsors/img8.jpg">
	            </div>
	        </div>
	        <div class='popup'>
	        	<div class='offclick'></div>
	        	<div class='holder'>
	        		<h4 class='close'>x</h4>
		        	<img src="">
		            <p></p>
	        	</div>
	        </div>
	    </section>
	</main>

	<script type="text/javascript">
	    logos = {
	        'logo_amp': {
	            p: 'Ampcus is a global business and technology solutions and staffing firm providing technical/non-technical resources to clients across multiple industry segments in the public and private sector.  Ampcus assists clients with their business, technology, and transformational journey by also providing SOW services specializing in digital transformation, infrastructure modernization, cybersecurity/risk management, testing, and IV and V.'
	        },
	        'logo_compunnel': {
	            p: 'Compunnel is a trusted human capital management services provider offering workforce hiring, payroll, and RPO services for leading businesses including major Fortune 500 businesses and eminent startups. Compunnel is an early technology adopter in the industry and has devised several technology solutions to help businesses find and retain top talent.'
	        },
	        'logo_enterprise': {
	            p: 'Enterprise Solutions is a MBE-certified partner for AgileOne. As an organization they partner with 43 current MSPs, with a 24/7 recruiting team consisting of over 228 verticalized recruiters. The company currently supports 41 MSP programs in aviation, banking and finance, energy, solar, manufacturing, electronics, telecom, insurance, retail, technology, life sciences, healthcare, casino, pharma, government, and automotive. The company supports many roles including IT, engineering, non-IT, professional, technical, accounting/finance, banking, light industrial, manufacturing, light technical, clerical, administrative, call center, pharma, randd, systems integration, SOW, project solutions, payroll services, and more.'
	        },
	        'logo_eteam': {
	            p: 'eTeam provides recruitment and executive search, high-volume staffing, managed services (SOW, RPO), independent consultant vetting, and payrolling services to structured contingent workforce programs and projects across AMER, EMEA, and APAC.  eTeam’s business model is geared towards servicing and succeeding in an VMS/MSP/self-managed structured contingent program model while engaged at more than 150 national and international accounts.'
	        },
	        'logo_HKA': {
	            p: 'Although HKA Enterprises considers itself a specialist in the engineering, operations and maintenance, power and utilities, automotive, pharmaceutical, skilled crafts, and manufacturing, the company’s focus is being a strategic and transparent partner to AgileOne and the end client by providing quality workforce solutions and reducing overall costs.'
	        },
	        'logo_InSync': {
	            p: 'inSync provides a staffing delivery model proven to succeed in competitive managed service program environments. inSync’s five highest volume placement categories are admin/clerical, customer service/call center, light industrial/manufacturing, professional (accounting, finance, procurement, marketing, HR) and scientific/lab.'
	        },
	        'logo_IntegratedResources': {
	            p: 'IRI has been in business for more than 20 years, with the last nine years focused on delivering MSP staffing solutions. The company’s primary focus is on life sciences, healthcare, consumer goods, and utilities verticals. They are a minority business enterprise and joint commission certified healthcare staffing firm. '
	        },
	        'logo_MSI': {
	            p: 'Millennium has done over half a billion in business with over 100 of the Fortune 500, while always putting Ethics before Profits. Their focus includes IT, engineering, professional, technical/healthcare, and light industrial.'
	        },
	        'logo_Net2Source': {
	            p: 'Net2Source is a true global workforce solutions company with brick and mortar operations in more than 20 countries. The company delivers differentiating and winning talent solutions to clients ranging from Fortune 100 to Global 2000 in 54+ countries including, but not limited to, North America, LATAM, South America, EMEA, ANZ, UAE, and JAPAC. They offer services ranging from SOW, contingent staffing, project staffing, RPO, IC, and payrolling. Their areas of expertise lie in IT and engineering, research and development, allied healthcare, HIT and HIMS, accounting and finance, HR and admin, professional engineering, and light industrial. '
	        },
	        'logo_OnBoardServices': {
	            p: 'On-Board’s Family of Companies have core competencies in four major markets, with On-Board Services (OBS) supporting professional staffing opportunities. OBS specializes in three major categories direct placement, MSP/VMS programs, and industrial outsourcing services.'
	        },
	        'logo_Radiant': {
	            p:'Radiant Systems, Inc. is a minority business enterprise that specializes in providing staff augmentation, SOW, consulting, and payrolling services across USA, Canada, UK, Netherlands, Switzerland, and India. For the last 23 years they have efficiently addressed talent acquisition needs, and their diversity program initiatives provide them with a significant strength to support their clients and perform in the markets they serve.'
	        },
	        'logo_fountainGroup': {
	            p: 'The Fountain Group (TFG) specializes in providing contingent workforce solutions throughout the US and Canada. TFG’s competitive edge is in their educated recruiters, with an average tenure of over seven years. The TFG family works through strategic partnerships to understand all their clients’ needs and cultures to provide award-winning, quality talent.'
	        },
	        'logo_Judge': {
	            p: 'Entering its 50th year of service, The Judge Group is a leading professional services firm specializing in talent, technology, and learning solutions. Serving more than 52 of the Fortune 100 with over 9,000 professionals working across a wide range of industries, Judge delivers its solutions through a network of 35 offices in the US, Canada, and India.'
	        },
	        'logo_TPG': {
	            p: 'The Planet Group brand has a global presence and is comprised of seven child companies. They have built their organization to provide high-value outsourced human capital solutions and expert consulting services within the life sciences, diversified energy, engineering, healthcare, IT, and digital marketing industries.'
	        }
	    }

	    $(document).on('click', '.card', function(){
	    	$(this).addClass('enlarge');
	        $(this).addClass('open');
	        var id = $(this).attr('data-id');
	        var src = $(this).children('img').attr('src');
            $('.popup p').html(logos[id].p);
            $('.popup img').attr('src', src);
	        var timer = setTimeout(function(){
	            $('.popup').fadeIn(300);
	            $('.popup').css('display', 'flex');
	        }, 500);
	    });

	    $(document).on('click', '.offclick, .close', function(){
	    	$('.popup').fadeOut(300);
	    });
	</script>
</main>