<main class='content'>
	<div class='home'>
		<div class='cards'>
			<a id='speakers' class='card full'>
				<p>Speakers <img class='arr' src="<?= FRONT_ASSETS ?>img/arr.png"></p>
				<img class='img' src="<?= FRONT_ASSETS ?>img/speakers.png">
			</a>
			<a id='agenda' class='card half'>
				<img src="<?= FRONT_ASSETS ?>img/agenda.png">
				<p>Today’s Agenda</p>
			</a>
			<a id='sponsors' class='card half'>
				<img src="<?= FRONT_ASSETS ?>img/sponsors.png">
				<p>Today’s Sponsors</p>
			</a>
			<a id='floorplan' class='card half'>
				<img src="<?= FRONT_ASSETS ?>img/map.png">
				<p>Floor Plan</p>
			</a>
			<a id='allsponsors' class='card half'>
				<img src="<?= FRONT_ASSETS ?>img/sponsors.png">
				<p>All Sponsors</p>
			</a>
			<a id='awardees' class='card full awardees'>
				<p>Awardees <img class='arr' src="<?= FRONT_ASSETS ?>img/arr.png"></p>
				<img class='img' src="<?= FRONT_ASSETS ?>img/awardees.png">
			</a>
		</div>
	</div>
</main>