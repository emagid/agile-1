<div class='content'>
  <link rel = "stylesheet" type = "text/css" href = "<?=FRONT_CSS?>photobooth.css">
  <a class='go_home' href="/"><img src="<?= FRONT_ASSETS ?>img/home.png"></a>
    <body>
      <main>
        <div class='background'></div> 
        <section class="home_page photobooth" >
          <div class='pic_choices bottom_row'>
            <div id='take_pic_b' class='gold'>
              <i class="fas fa-camera"></i>
              <p>Take a <span>PIC</span></p>
            </div>
            <div id='snap_gif_b' class='black'>
              <i class="fas fa-camera"></i>
              <p>Take a <span>GIF</span></p>
            </div>
          </div>
              <!-- home button -->
        <a href="/"><aside id='home_click_white' class='home_click'>
            <img class='white_img' src="<?=FRONT_ASSETS?>img/home.png"> 
        </aside></a>

    <body>
      <!--  ==========  PHOTOS  =============== -->
      <section id='photos' class='photos'>

        <!-- Cam -->
        <h3 class='pic_text'>Choose an option below and look up!</h3>
        <img id='filter' src="<?=FRONT_ASSETS?>img/earth.png">
        <video id="video" width="1080px" height="1700px" autoplay muted></video>
                <device type="media" onchange="update(this.data)"></device>

                <script>
                  var video = document.getElementById('video');

                  if(navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
                      navigator.mediaDevices.getUserMedia({ video: true }).then(function(stream) {
                          try {
                            video.src = window.URL.createObjectURL(stream);
                          } catch(error) {
                            video.srcObject = stream;
                          }
                      try {
                            video.play();
                      } catch (error) {
                        console.log(error)
                          }
                      });
                  }
            </script>

          <!-- countdown --><div class='countdown'>3</div>
          <!-- countdown --><div class='countdown'>2</div>
          <!-- countdown --><div class='countdown'>1</div>
          <!-- flash --><div class='flash'></div>
      </section>

      <!-- Choosing pictures -->
      <div id='pictures'>
                <!-- <img id='pen' src="<?= FRONT_ASSETS ?>img/draw.png"> -->

                <div class='draw_ctl drawing'>
                    <i class="fa fa-close" style="font-size:36px"></i>
                </div>

                <div class='colors drawing'>
                    <div class='color' id='colorRed'></div>
                    <div class='color' id='colorGreen'></div>
                    <div class='color' id='colorBlue'></div>
                    <div class='color' id='colorWhite'></div>
                    <div class='color' id='colorBlack'></div>
                </div>


                <div class='sizes drawing'>
                    <div class='size' id='small'></div>
                    <div class='size' id='medium'></div>
                    <div class='size' id='large'></div>
                    <div class='size' id='huge'></div>
                </div>

                 <!-- <div  id='canvasDiv'></div> -->


        <div class='button next'>NEXT</div>
        <p class='gif_info'>Choose 4 photos to create your GIF</p>
        
      <div class='pic_options bottom_row'>
          <a href="/home/photobooth"><div id='take_pic_b'>
            <p>Click to <span>RETAKE</span></p>
          </div>
          </a>
          <div class='submit black'>
            <i class="fas fa-camera"></i>
            <p>Click to <span>SHARE</span></p>
          </div>
        </div>
        <div class='container gif_container'></div>
      </div>

    

      <!-- Showing Gif -->
      <section class='gif_show'>
        <img id='loader' src="<?=FRONT_ASSETS?>img/loader.gif">
        <div class='pic_options bottom_row'>
            <a href="/home/photobooth"><div id='take_pic_b'>
              <p>Click to <span>RETAKE</span></p>
            </div>
            </a>
            <div class='submit black'>
              <i class="fas fa-camera"></i>
              <p>Click to <span>SHARE</span></p>
            </div>
          </div>
          <div class='container gif_container'></div>
        </div>
      </section>

      <!-- Share overlay -->
      <i class="fa fa-close sharex" style="font-size:36px"></i>
      <section class='share_overlay'>
        <div class='container'>
          <form id='submit_form'>
            <input type='hidden' name='form' value="1">
                        <input type="hidden" name="image" class="image_encoded">
                        <span>
                            <input class='input jQKeyboard first_email' name='email[]' pattern="[A-z0-9._%+-]+@[A-z0-9.-]+\.[A-z]{2,3}$" type='text' placeholder='Email' title='Please enter a valid email address.'>
                        </span>
                        <p id='add_email' style='color: #ffffffab;'>Add an email +</p>
                        <div class='line'></div>
                        <!-- <span>
                            <input class='input jQKeyboard first_phone' name="phone[]" placeholder="Phone Number (eg: +19874563210)">
                        </span>
                        <p id='add_phone' style='color: #ffffffab;'>Add a phone number +</p> -->
            <input id='share_btn' class='button reverse' type='submit' value='SHARE'>
                        <!-- <div class='button print'>PRINT</div> -->
          </form>
          <!-- <a href='/' id='done' class='button'>DONE</a> -->
        </div>
      </section>

      <!-- Alerts -->
      <section id='share_alert'></section>

      <div class='filters'>
        <img id='Confidence' src="<?=FRONT_ASSETS?>img/confidence.png">
        <img id='Conviction' src="<?=FRONT_ASSETS?>img/conviction.png">
        <img id='Dedication' src="<?=FRONT_ASSETS?>img/dedication.png">
        <img id='Giving' src="<?=FRONT_ASSETS?>img/giving.png">
        <img id='Respect' src="<?=FRONT_ASSETS?>img/respect.png">
        <img id='Spirituality' src="<?=FRONT_ASSETS?>img/spirituality.png">
      </div>

    </body>

    <!-- <div class='filters'>
      <img src="<?=FRONT_ASSETS?>img/frame1.png">
      <img src="<?=FRONT_ASSETS?>img/frame2.png">
    </div> -->

  </section>

</main>
</body>
 <!-- <script type="text/javascript" src="<?= FRONT_JS ?>html5-canvas-drawing-app.js"></script>
    <script type="text/javascript">
         drawingApp.init();
    </script> -->

            
<script type="text/javascript">
    $(document).ready(function(){

      $('.filter_btn').click(function(){
        $('.filters_holder').css('width', '900px');
        $('.off_click').fadeIn(300);
        setTimeout(function(){
          $('.filters_holder div').fadeIn(300);
        }, 500);
        $('.filters_holder div');
      });

      $('.off_click, .fa-close').click(function(){
        $('.filters_holder div').fadeOut();
        $('.filters_holder').css('width', '0');
        $('.off_click').fadeOut(300);
        setTimeout(function(){
          $('.filters_holder .fa-close').show();
        }, 1000)
      });

      $('.filters_holder h2').click(function(){
        $('.filters_holder h2').removeClass('filter_active');
        $(this).addClass('filter_active');
      });
    // alert('hi')
       
        var images = <?= \Model\Snapshot_Contact::slider()?>; //array of image urls
        var gifs =   <?= \Model\Gif::slider()?>; //array of gif urls
        // slideStart();
        addMedia(images, gifs);


        $('#add_email').click(function(){
            $('.first_email').parent().append("<input required class='input jQKeyboard' name='email[]' pattern='[A-z0-9._%+-]+@[A-z0-9.-]+\.[A-z]{2,3}$' type='text' placeholder='Email' title='Please enter a valid email address.'>");
            $('.first_email').parent().find('input').last().initKeypad({'keyboardLayout': keyboard});
        });

        $('#add_phone').click(function(){
            $('.first_phone').parent().append("<input required class='input jQKeyboard' name='phone[]' placeholder='Phone Number (eg: +19874563210)'>");
            $('.first_phone').parent().find('input').last().initKeypad({'keyboardLayout': keyboard});
        });

        $('#video').fadeIn(1000);

        setTimeout(function(){
            // $('.pic_text').fadeIn(2000);
            $('#take_pic').fadeIn(2000);
            $('#snap_gif').fadeIn(2000);
        }, 1000);

        setTimeout(function(){
            $('#video').fadeIn(1000);
        }, 2000);


        function addMedia(images, gifs) {
          console.log(images)

        for ( i=0; i<images.length; i++ ) {
          var div = "<img class='event_pic' src='" + images[i].replace('\\','/') + "'></div>";
          $('.event_pics .display_images').append(div);
        }
            
        for ( i=0; i<gifs.length; i++ ) {
              var div = "<img class='event_pic' src='" + gifs[i].replace('\\','/') + "'></div>";
            $('.event_pics .display_gifs').append(div);
        }
      }


      // ===============================================  PHOTOBOOTH CODE  ===============================================

         // Checkmark
      var count = (function (num) {
          var counter = 0;
          return function (num) {return counter += num;}
      })();
      

       $(document).on('click', '.gif', function(){
          var num = count(1);
          if ( !$(this).hasClass('checked') && num < 5 ) {            
              if ( num <= 4  ) {
                  $(this).css('outline', '#c9ac5e solid 5px')
                  $(this).addClass('checked');
                  if ( num  === 4 ) {
                      $('.gif_info').fadeOut();
                      $('.next').slideDown();
                  }
              }

          }else if ( $(this).hasClass('checked') ) {
              num = count(-2);
              $(this).removeClass('checked');
              $(this).css('outline', '0px')
              if ( num  === 4 ) {
                      $('.next').slideDown();
                      $('.gif_info').fadeOut();

              }else {
                  $('.next').slideUp();
                  $('.gif_info').fadeIn();

              }

          }else {
              num = count(-1);
          }
       });




       // Gif Next click
       function picAction( pic ) {
        $(pic).fadeIn('fast');
        $(pic).delay(1300).fadeOut(400);
      }

       function gif( pics ) {
        var offset = 0

        for ( i=0; i<2; i++ ) {
          pics.each(function(){
            var timer
            var self = this;
            timer = setTimeout(function(){
                picAction(self)
            }, 0 + offset);

            offset += 1500;
          });
        }

        setTimeout(function(){
              var gif_pics = $('.gif_show').children('.checked');
              gif( gif_pics )
          }, 1200);
      }

       


       $('.event_pics .overlay').on('click', function(){
        $('.photos').css('opacity', '0');


        $(this).children('h2').fadeOut(1500);

        setTimeout(function(){
            $('.inner_content').css('display', 'flex');
            $('.event_pics').css('height', '1920px');
            $('.event_pics').addClass('active');

            $('.photos').hide();
            $('.inner_content').slideDown(200);
            // $('.display_images').slideDown();
            $('.choice').css('opacity', '1');
            $('.bubble').hide();

        }, 1000);

        setTimeout(function(){
            $('#home_click').fadeIn();
            $('.event_pics').css('min-height', '1920px');
            $('.event_pics').css('touch-events', 'none');
        }, 1500);
    });
      


    // click of showing images
     $('.event_images').on('click', function(){
        $('.button').fadeOut();
        $('#event_gifs').fadeIn();
        $('.display_images').slideDown();
     });


      // click of showing gifs
     $('.event_gifs').on('click', function(){
        $('.button').fadeOut();
        $('#event_images').fadeIn();
        $('.display_gifs').slideDown();
     });


      // click of showing gifs when images are shown
     $('#event_gifs').on('click', function(){
        $('#event_images').fadeIn();
        $('.button').fadeOut();
        $('#event_gifs').fadeOut();
        $('.display_images').slideUp();
        setTimeout(function(){
            $('#event_images').fadeIn();
            $('#event_images').addClass('shrunk');
            $('.display_gifs').slideDown();
        }, 1000)
     });

     // click of showing images when gifs are shown
     $('#event_images').on('click', function(){
        $('.button').fadeOut();
        $('.display_gifs').slideUp();
        setTimeout(function(){
            $('#event_gifs').fadeIn();
            $('#event_gifs').addClass('shrunk');
            $('.display_images').slideDown();
        }, 1000)
     });



       // Share click
       $(document).on('click', '.submit', function(){
         
          if ( $('#draw').is(':visible') ) {
               saveDrawing();


              $.post('/contact/save_img/',{image:$('#pictures > img:last-child').attr('src')},function(data){
                  $('#submit_form input[name=image]').val(data.image.id);
              });
          }

          $('.share_overlay').slideDown();
          $('.fa-close').fadeIn();
       });

        $('#share_btn').click(function(){
          $('.fa-close').fadeOut();
          $('.jQKeyboardContainer').fadeOut();
       })

       function saveDrawing() {
              
              var draw_canvas = $('#draw');
              $($(draw_canvas[0]).parents('#pictures')[0]).append(convertCanvasToImage(draw_canvas[0]))

              $('.drawing').hide();
          }
       


       // Form submit
       $('#submit_form').on('submit', function(e){
          e.preventDefault();
          $('#share_alert').fadeIn(1000);
          $.post('/contact', $(this).serialize(), function (response) {
              if(response.status){
                  $('#submit_form')[0].reset();
                  setTimeout(function(){
                    window.location.href = '/';
                  }, 6000);
              }else {
                setTimeout(function(){
                    window.location.href = '/';
                  }, 6000);
              }
          });
       });


   $('.fa-close').click(function(){
        $('.share_overlay').slideUp();
        $('#jQKeyboardContainer').slideUp();
        $('.fa-close').fadeOut();
    });


       // Click of done sharing
       $('#done').on('click', function(e){
          $('.share_overlay').slideUp();
          $('#jQKeyboardContainer').remove()
          // remove all images
          $('#pictures').children('.canvas_holder');
       });


       $('.print').click(function(){
            saveDrawing();  
            var source = $($('#pictures img:last-child')[1]).attr('src');
            VoucherPrint(source);
       });


       function convertCanvasToImage(canvas) {
          var image = new Image();
          image.src = canvas.toDataURL("image/png");
          console.log(image)
          return image;
      }





      // ===========  CAMERA FUNCTION  ==============

    // Grab elements, create settings, etc.
    var video = document.getElementById('video');

    // Get access to the camera!
    if(navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
        // Not adding `{ audio: true }` since we only want video now
        navigator.mediaDevices.getUserMedia({ video: true }).then(function(stream) {
            video.src = window.URL.createObjectURL(stream);
            video.play();
        });
    }

 
    function countDown() {
        var divs = $('.countdown');
        var timer
        var offset = 0 

        divs.each(function(){
            var self = this;
            timer = setTimeout(function(){
                $(self).css('font-size', '250px');
                $(self).fadeOut(1000);
            }, 1000 + offset);
            offset += 1000;
        });

        // flash
        setTimeout(function(){
            $('.flash').fadeIn(200);
            $('.flash').fadeOut(400);
            $(divs).css('font-size', '0px');
        }, 4000);
    }


    function gifPhotos() {
      $('.filter_btn').hide();
        var divs = $('.countdown');
        var offset = 1000;

        for ( i=0; i<3; i++) {

            (function(i){
              setTimeout(function(){
                    $('#pictures .gif_container').append("<div class='canvas_holder gif'><canvas class='canvas' width='900' height='675'></canvas></div>")
                    var canvas = $('.canvas');
                    var filter = $('#filter')[0];
                    var context = canvas[canvas.length -1].getContext('2d');
                    var video = document.getElementById('video');
                    var background = $('#background')[0];

                     $('.flash').fadeIn(200);
                    $('.flash').fadeOut(400);

                    $(divs).css('font-size', '0px');

                   context.drawImage(video, 0, 0, 900, 675);
                   context.drawImage(filter, 0, 0, 900, 675);
            
                   $($(canvas[canvas.length -1]).parents('.canvas_holder')[0]).css('display', 'inline-block');

                   // watermark
                    // var img2 = new Image();
                    // img2.src = '/content/frontend/assets/img/logo_holder.png';
                    // context.drawImage(img2,530,420, 80, 43);

                    $($(canvas[canvas.length -1]).parents('.canvas_holder')[0]).html(convertCanvasToImage(canvas[canvas.length -1]))
              
              }, offset);
              offset += 1000
            }(i));  
        }
    }


    function showGifs() {
        $('#pictures').fadeOut();
        $('.gif_show').fadeIn();
        $('.gif_show').addClass('flex');

        $('#submit_form').find('.image_encoded').remove();
          $('.gif img').each(function () {
              $('#submit_form').append($('<input type="hidden" name="images[]" value="'+$(this).attr('src')+'">'));
          });
          let deferreds = [];
          let imgs = [];

          $("#submit_form input[name='images[]']").each(function(i,el){

              deferreds.push(
                  $.post('/contact/make_frame/', {
                      image: el.value
                  },function(data) {
                    // Need to manually type in the url
                      var newref = "https://earthday2019.popshap.net/"
                      imgs.push(newref+'content/uploads/Snapshots/'+data.image.image);
                      $('input[name="images[]"]')[i].value = data.image.id;
                  }));
          });

          $('.fa-close').click(function(){
            $('.share_overlay').slideUp();
            $('.jQKeyboardContainer').slideUp();
            $(this).fadeOut();
          });


          //Show loading image
          $.when(...deferreds).then( function() {
               gifshot.createGIF({
                   'images': imgs,
                   'frameDuration':4,
                   'gifWidth': 900,
                   'gifHeight': 675
               },function(obj) {
                   if(!obj.error) {
                       var image = obj.image;
                       $('#loader').fadeOut();
                       $('.pic_options').fadeIn();
                       $('.pic_options').css('display', 'flex');
                       $('.button.submit').fadeIn();
                       $('.gif_show').append("<img src=' " + image + "'>")
                       $('#submit_form').append($('<input type="hidden" name="gif" value="'+image+'">'));
                       $.post('/contact/save_img/',$('#submit_form').serialize(),function(data){
                           $('#submit_form input[name=gif]').val(data.gif.id);
                       });
                   }
               });
           });

    }
                    




    function convertCanvasToImage(canvas) {
        var image = new Image();
        image.src = canvas.toDataURL("image/png");
        console.log(image)
        return image;
    }

    function convertCanvasToImage2(canvas) {
        var dataURL = canvas.toDataURL();
        return canvas.src = dataURL;
    }

    


    function showPictures() {
        $('#pictures').fadeIn(2000);
        var photos = $('#pictures').children('.photo');
            $(photos[0]).fadeIn();

        $.post('/contact/save_img/',{image:$($(photos[0]).html()).attr('src')},function(data){
            $('#submit_form input[name=image]').val(data.image.id);
        });

        // $('#pen').fadeIn();
    }



    // GIF CLICK
    $("#snap_gif, #snap_gif_b").on("click", function() {
        $('.pic_choices, .pic_text').fadeOut(1000);
        $('.pic_choices').css('pointer-events', 'none');
        var pics = []

        countDown();
        setTimeout(function(){
            $('#pictures .gif_container').append("<div class='canvas_holder gif'><canvas class='canvas' width='900' height='675'></canvas></div>")

                var canvas = $('.canvas');
                var filter = $('#filter')[0];
                var context = canvas[canvas.length -1].getContext('2d');
                var video = document.getElementById('video');
                var background = $('#background')[0];
               context.drawImage(video, 0, 0, 900, 675);
               context.drawImage(filter, 0, 0, 900, 675);
               

               $($(canvas[canvas.length -1]).parents('.canvas_holder')[0]).css('display', 'inline-block');
               $($(canvas[canvas.length -1]).parents('.canvas_holder')[0]).html(convertCanvasToImage(canvas[canvas.length -1]))


           gifPhotos();

        }, 4000 )

        setTimeout(function(){
            $('#photos').css('opacity', '0');
            setTimeout(function(){
                $('#photos').fadeOut();
            }, 1100);

            showGifs();
            $(this).css('pointer-events', 'all');
        }, 8000);
    });



    // PHOTO CLICK
    $('#take_pic, #take_pic_b').click(function(){
        $('.pic_choices, .pic_text').fadeOut(1000);
        $('.pic_choices').css('pointer-events', 'none');
        var pics = []

        countDown();
            // take the picture
            setTimeout(function(){
                $('#pictures').append("<div class='canvas_holder photo'><canvas id='pic' class='canvas' width='900' height='675'></canvas></div>")
                
                var canvas = $('#pic');
                var filter = $('#filter')[0];
                var context = canvas[canvas.length -1].getContext('2d');
                var video = document.getElementById('video');
                var background = $('#background')[0];
               context.drawImage(video, 0, 0, 900, 675);
               context.drawImage(filter, 0, 0, 900, 675);
               

                $($(canvas[canvas.length -1]).parents('.canvas_holder')[0]).html(convertCanvasToImage(canvas[canvas.length -1]))


                $('#pictures').append("<canvas id='draw' class='drawing' width='900' height='675'></canvas>");
                var drawcanvas = $('#draw');
                var ctx = drawcanvas[0].getContext('2d');
                var video = document.getElementById('video');
                var background = $('#background')[0];
               ctx.drawImage(video, 0, 0, 900, 675);
               // ctx.drawImage(background, 0, 0, 900, 675);






            }, 4000);

        setTimeout(function(){
            $('#photos').slideUp()
            $('.submit, .retake').delay(1000).fadeIn();
            $('.print').delay(1000).fadeIn();
            $(this).css('pointer-events', 'all');
            showPictures();
        }, 5000);
    });



    // DRAWING
    $('#pen').click(function(){
        // $('.canvas_holder').append("<canvas id='draw' width='900' height='675'></canvas>")
       context = document.getElementById('draw').getContext("2d");
        $('#draw').fadeIn()
        $('.draw_ctl').fadeIn();
        $('.colors').fadeIn();
        $('.sizes').fadeIn();
        $('#pictures .canvas_holder img').hide();

        can = document.getElementById('draw');
        can.addEventListener('mousedown', onMouseDown);
        can.addEventListener('touchstart', onTouchStart);
        can.addEventListener('mousemove', onMouseMove);
        can.addEventListener('touchmove', onTouchMove);
        can.addEventListener('mouseup', onMouseEnd);
        can.addEventListener('touchend', onMouseEnd);
        can.addEventListener('mouseleave', onMouseEnd);
        can.addEventListener('touchleave', onMouseEnd);
        $(this).fadeOut();
    });



    function onMouseDown (e){

      var mouseX = e.pageX - this.offsetLeft;
      var mouseY = e.pageY - this.offsetTop;
      console.log(e)

            
      paint = true;
      addClick(e.clientX - this.offsetLeft, e.clientY - this.offsetTop);
      redraw();
      onTouchMove(e);
    };

    function onTouchStart (e){
        addClick(e.touches[0].screenX - this.offsetLeft, e.touches[0].screenY - this.offsetTop);
        redraw();
      
    };

    function onMouseMove (e){
        if(paint){
            addClick(e.pageX - this.offsetLeft, e.pageY - this.offsetTop, true);
            redraw();
            onTouchMove(e)
      }
      
    };

    function onTouchMove (e){
        addClick(e.touches[0].screenX - this.offsetLeft, e.touches[0].screenY - this.offsetTop, true);
        redraw();
    };

    function onMouseEnd (e){
         paint = false;
    };



    var clickX = new Array();
    var clickY = new Array();
    var clickDrag = new Array();
    var paint;
    var colorWhite = "#ffffff";
    var colorGreen = "#49a463";
    var colorBlue = "#30b9dc";
    var colorBlack = "#000000";
    var colorRed = "#c22628";

    var curColor = colorRed;
    var clickColor = new Array();

    $('#colorWhite').click(function(){
        curColor = colorWhite
    });

    $('#colorGreen').click(function(){
        curColor = colorGreen
    });

    $('#colorBlue').click(function(){
        curColor = colorBlue
    });

    $('#colorBlack').click(function(){
        curColor = colorBlack
    });

    $('#colorRed').click(function(){
        curColor = colorRed
    });

    $('.color').click(function(){
        $('.canvas_holder').css('background-color', curColor);
        setTimeout(function(){
            $('.canvas_holder').css('background-color', 'transparent');
        }, 200)
    });


    var small = 5;
    var medium = 13;
    var large = 20;
    var huge = 30;

    var curSize = medium;
    var clickSize = new Array();

    $('#small').click(function(){
        curSize = small;
    });

    $('#medium').click(function(){
        curSize = medium;
    });

    $('#large').click(function(){
        curSize = large;
    });

    $('#huge').click(function(){
        curSize = huge;
    });



    

    function addClick(x, y, dragging)
    {
      clickX.push(x);
      clickY.push(y);
      clickDrag.push(dragging);
      clickColor.push(curColor);
      clickSize.push(curSize);
    }

    function redraw(){
      // context.clearRect(0, 0, context.canvas.width, context.canvas.height); // Clears the canvas
      
      context.strokeStyle = "#df4b26";
      context.lineJoin = "round";
      

                
      for(var i=0; i < clickX.length; i++) {        
        context.beginPath();
        if(clickDrag[i] && i){
          context.moveTo(clickX[i-1], clickY[i-1]);
         }else{
           context.moveTo(clickX[i]-1, clickY[i]);
         }
         context.lineTo(clickX[i], clickY[i]);
         context.closePath();
         context.strokeStyle = clickColor[i];
         context.lineWidth = clickSize[i];
         context.stroke();
      }
    }


    $('.draw_ctl').click(function(){
      clickX = new Array();
      clickY = new Array();
      clickDrag = new Array();
      clickColor = new Array();
      clickSize = new Array();
      context.clearRect(0, 0, 900, 675);

      var drawcanvas = $('#draw');
      var ctx = drawcanvas[0].getContext('2d');
      var image = $('#pictures .canvas_holder img')[0];
       ctx.drawImage(image, 0, 0, 900, 675);
    });
            


    $('.filters_holder h2').on("click", function() {
      if ( !$('.useImage').is(':visible') ) {
        $('.canvas_holder img').addClass('useImage');
        $('.filters').append($('.useImage'));
      }

      setTimeout(function(){
        $('.filters_holder div').fadeOut();
        $('.filters_holder').css('width', '0');
        $('.off_click').fadeOut(300);
        setTimeout(function(){
          $('.filters_holder .fa-close').show();
        }, 1000)
      }, 500);

      $('.canvas_holder').append("<canvas id='pic' class='canvas' width='900' height='675'></canvas>")
      
      var canvas = $('#pic');
      var context = canvas[canvas.length -1].getContext('2d');
      var video = $('.useImage')[0];
      var filter = $('#' + $(this).html())[0];
      context.drawImage(video, 0, 0, 900, 675);
      context.drawImage(filter, 0, 0, 900, 675);
     

      $($(canvas[canvas.length -1]).parents('.canvas_holder')[0]).html(convertCanvasToImage(canvas[canvas.length -1]))

      setTimeout(function(){
          $(this).css('pointer-events', 'all');
          showPictures();
      }, 5000);
    });

});
   
</script>
</div>
