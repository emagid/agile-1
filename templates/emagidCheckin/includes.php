
<link rel = "stylesheet" type = "text/css" href = "<?=FRONT_CSS?>main.css">
<link rel = "stylesheet" type = "text/css" href = "<?=FRONT_CSS?>photobooth.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="<?=FRONT_CSS?>jQKeyboard.css">
<link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700,700i" rel="stylesheet">
<link rel = "stylesheet" href = "https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
<link rel = "shortcut icon" href = "<?=FRONT_ASSETS?>img/american-favicon.png">
<script>
    if(localStorage.hasOwnProperty('kiosk')){

        var customData = {
            'kiosk' : localStorage.kiosk
        }

        window._loq = window._loq || []
        window._loq.push(['custom', customData])
    }
</script>
<script type='text/javascript'>
window.__lo_site_id = 113230;

    (function() {
        var wa = document.createElement('script'); wa.type = 'text/javascript'; wa.async = true;
        wa.src = 'https://d10lpsik1i8c69.cloudfront.net/w.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(wa, s);
      })();
</script>

<script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="https://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script src="https://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script src="<?=FRONT_LIBS?>/timeit/timeit.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script src="<?=FRONT_JS?>main.js"></script>
<script src="<?=FRONT_JS?>camera.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="<?=FRONT_LIBS?>gifshot/gifshot.js"></script>



<? if ($this->emagid->route['controller'] == 'products' || $this->emagid->route['controller'] == 'weddingbands' || $this->emagid->route['controller'] == 'rings' || $this->emagid->route['controller'] == 'jewelry') { ?>
    <link rel = "stylesheet" type = "text/css" href = "<?=FRONT_CSS?>categories.css">
    <script src="<?=FRONT_JS?>category.js"></script>
    <link rel = "stylesheet" type = "text/css" href = "<?=FRONT_CSS?>product.css">
    <script src="<?=FRONT_JS?>product.js"></script>
<? } ?>